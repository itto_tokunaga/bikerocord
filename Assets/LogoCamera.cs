﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoCamera : MonoBehaviour {

    [SerializeField]
    GameObject bg;

    Material bgMat;

    [SerializeField]
    SpriteRenderer logoSprite;

    [SerializeField]
    float fadeSecond = 1.0f;

    float alpha = 0.0f;

    public enum STATE
    {
        WAIT,
        FADE_IN,
        FADE_OUT
    }
    STATE state;

    // Use this for initialization
    void Start () {
        bgMat = bg.GetComponent<Renderer>().material;
    }
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case STATE.WAIT:
                alpha = 0.0f;
                break;
            case STATE.FADE_IN:
                alpha += 1.0f * Time.deltaTime / fadeSecond;
                break;
            case STATE.FADE_OUT:
                alpha -= 1.0f * Time.deltaTime / fadeSecond;
                break;
            default:
                break;
        }

        alpha = Mathf.Clamp01(alpha);

        Color c = bgMat.color;
        c.a = alpha;
        bgMat.color = c;

        c = logoSprite.color;
        c.a = alpha;
        logoSprite.color = c;
    }


    public void FadeIn()
    {
        state = STATE.FADE_IN;
    }
}
