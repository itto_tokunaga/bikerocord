﻿Shader "Custom/Shader_Smoothness" {
	Properties {
		_MainTex    ("Albedo (RGB)", 2D         ) = "white" {}
		_Color      ("Color"       , Color      ) = (1, 1, 1, 1)
		_Metallic   ("Metallic"    , Range(0, 1)) = 0.0
		_SmoothMin  ("Smooth Min"  , Range(0, 1)) = 0.0
		_SmoothMax  ("Smooth Max"  , Range(0, 1)) = 1.0
	}

	SubShader {
		Tags {
			"Queue"      = "Geometry"
			"RenderType" = "Opaque"
		}
		
		CGPROGRAM
			#pragma target 3.0
			#pragma surface surf Standard fullforwardshadows

			sampler2D _MainTex;
			fixed3 _Color;
			half _Metallic;
			half _SmoothMin;
			half _SmoothMax;

			struct Input {
				float2 uv_MainTex;
			};

			void surf (Input IN, inout SurfaceOutputStandard o) {
				fixed4 mainTex = tex2D(_MainTex, IN.uv_MainTex);

				o.Albedo     = mainTex.rgb * _Color;
				o.Metallic   = _Metallic;
				o.Smoothness = mainTex.a * (_SmoothMax - _SmoothMin) + _SmoothMin;
				o.Alpha      = 1;
			}
		ENDCG
	}

	FallBack "Diffuse"
}