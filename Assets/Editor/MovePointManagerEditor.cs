﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovePointManager))]//拡張するクラスを指定
public class MovePointManagerEditor : Editor
{
    /// <summary>
    /// InspectorのGUIを更新
    /// </summary>
    public override void OnInspectorGUI()
    {
        //元のInspector部分を表示nt
        base.OnInspectorGUI();

        //ボタンを表示
        if (GUILayout.Button("UpdateMovePoint"))
        {
            //targetを変換して対象を取得
            MovePointManager manager = target as MovePointManager;
            manager.AutoSetChildrenPoint(manager.GetComponentsInChildren<MovePoint>());
        }
    }

}
