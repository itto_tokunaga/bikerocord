﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveControllerInput : MonoBehaviour {

    bool isTrigger;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        isTrigger = Input.GetMouseButtonDown(0);
    }

    public bool IsTrigger { get { return isTrigger; } }
}
