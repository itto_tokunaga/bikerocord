﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIMeter : MonoBehaviour
{
    const float SPEED_MAX = 407.0f;
    const int   METER_VALUE_NUM = 11;
    const int   OUTSIDE_ICON_NUM = 16;

    [SerializeField]
    Motorcycle motorcycle;

    [SerializeField]
    GameObject meterBar;

    [SerializeField]
    Text displayValueText;

    [SerializeField]
    GameObject[] meterValueLightObj = new GameObject[METER_VALUE_NUM];

    [SerializeField]
    GameObject[] outsideIconObj = new GameObject[OUTSIDE_ICON_NUM];

    int meterLightValueIndex = 0;
    int preMeterLightValueIndex = -1;


    int outsideIconFlashIndex = 0;

    float outsideIconFlashWaitMax = 0.5f;
    float outsideIconFlashWait = 0.0f;

    bool isFlash = false;


    // エンジン回転数
    float engineSpeed = 0;

    float targetRot;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateDisplayValue();
        UpdateMeterLightRot();

        if (!isFlash) UpdateMeterLightValue();

        OutsideIconsRandomFlash();
    }


    public void StartFlash()
    {
        if (isFlash) return;
        StartCoroutine(MeterValueFlash());
    }

    IEnumerator MeterValueFlash()
    {
        isFlash = true;

        float flashCount = 7;
        float flashWait = 0.05f;

        for (int i = 0; i < flashCount; i++)
        {
            yield return new WaitForSeconds(flashWait);
            ExtinctionMeterLightValue();
            yield return new WaitForSeconds(flashWait);
            ForceUpdateMeterLightValue();
        }

        isFlash = false;
    }

    /// <summary>
    /// ディスプレイ側の数字の点灯制御
    /// </summary>
    void UpdateDisplayValue()
    {
        displayValueText.text = ((int)(motorcycle.DisplaySpeed)).ToString();
    }


    /// <summary>
    /// メーターのバーの回転制御
    /// </summary>
    void UpdateMeterLightRot()
    {
        Quaternion rot = meterBar.GetComponent<RectTransform>().rotation;

        float goal = -(motorcycle.EngineSpeed * (240.0f / 16000.0f)) + 23;
        targetRot -= (targetRot - goal) * 0.5f;

        // 現在のメーターの角度から点灯すべき数値を判断
        //meterLightValueIndex = (int)((motorcycle.DisplaySpeed / SPEED_MAX) * (meterValueLightObj.Length + 1));
        UpdateMeterIndexByRot(targetRot);

        meterBar.GetComponent<RectTransform>().rotation = Quaternion.Euler(rot.eulerAngles.x, rot.eulerAngles.y, targetRot);
    }

    /// <summary>
    /// メーター側の数字の点灯制御
    /// </summary>
    void UpdateMeterLightValue()
    {
        if (preMeterLightValueIndex == meterLightValueIndex) return;
        for (int i = 0; i < meterValueLightObj.Length; i++)
        {
            if (i <= meterLightValueIndex) meterValueLightObj[i].SetActive(true);
            else meterValueLightObj[i].SetActive(false);
        }
    }

    /// <summary>
    /// 強制：メーター側の数字の点灯
    /// </summary>
    void ForceUpdateMeterLightValue()
    {
        for (int i = 0; i < meterValueLightObj.Length; i++)
        {
            if (i <= meterLightValueIndex) meterValueLightObj[i].SetActive(true);
            else meterValueLightObj[i].SetActive(false);
        }
    }

    /// <summary>
    /// メーター側の数字の全消し
    /// </summary>
    void ExtinctionMeterLightValue()
    {
        for (int i = 0; i < meterValueLightObj.Length; i++)
        {
            meterValueLightObj[i].SetActive(false);
        }
    }
    /// <summary>
    /// 現在のメーターの角度から点灯すべき数値を計算
    /// </summary>
    void UpdateMeterIndexByRot(float _rot)
    {
        if (isFlash) return;

        float[] border =
        {
            23.0f,
            8.0f,
            -19.0f,
            -33.0f,
            -60.0f,
            -89.0f,
            -118.0f,
            -149.0f,
            -180.0f,
            -212.0f
        };

        preMeterLightValueIndex = meterLightValueIndex;
        meterLightValueIndex = 0;
        for (int i = 0; i < border.Length; i++)
        {
            if (border[i] > _rot) meterLightValueIndex = i+1;
        }
  
    }

    /// <summary>
    /// 外側アイコンを適当に点灯
    /// </summary>
    void OutsideIconsRandomFlash()
    {
        //outsideIconFlashWait += Time.deltaTime;
        //if (outsideIconFlashWait > outsideIconFlashWaitMax)
        //{
        //    outsideIconFlashWait = 0.0f;
        //    outsideIconFlashIndex--;

        //    outsideIconFlashIndex = (outsideIconFlashIndex < 0) ? outsideIconObj.Length -1 : outsideIconFlashIndex;
        //}

        //for (int i = 0; i < outsideIconObj.Length; i++)
        //{
        //    outsideIconObj[i].SetActive(false);
        //}
        //outsideIconObj[outsideIconFlashIndex].SetActive(true);
        //outsideIconObj[1].SetActive(true);
    }
}
