﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public enum SOUND_ID
    {
        ENGINE_000,
        IDLING_LOOP,
        IDLING_000,
        IDLING_001,
        IDLING_002,
    };

    [SerializeField]
    AudioSource[] audioSources; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Play(SOUND_ID _id)
    {
        audioSources[(int)_id].Play();
    }

    public void Stop(SOUND_ID _id)
    {
        audioSources[(int)_id].Stop();
    }
}
